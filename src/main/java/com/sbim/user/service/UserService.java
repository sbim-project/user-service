package com.sbim.user.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.sbim.user.model.User;
import com.sbim.user.util.UserResponse;

@Service
public interface UserService {

	List<User> getAllUsers();

	User saveUser(User user);

	User updateUser(User user);

	User getUserById(String userid);

	String deleteUserById(String userid);

	UserResponse verifyUser(User user);

}
