package com.sbim.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbim.user.model.User;
import com.sbim.user.repository.UserRepository;
import com.sbim.user.util.UserResponse;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Override
	public List<User> getAllUsers() {

		return userRepository.findAll();
	}

	@Override
	public User saveUser(User user) {
		return userRepository.save(user);
	}

	@Override
	public User updateUser(User user) {
		return userRepository.save(user);
	}

	@Override
	public User getUserById(String userid) {
		return userRepository.getById(userid);
	}

	@Override
	public String deleteUserById(String userid) {

		User user = userRepository.findByUserId(userid);

		userRepository.delete(user);

		return userid;

	}

	@Override
	public UserResponse verifyUser(User user) {

		UserResponse response = new UserResponse();
		User user2 = userRepository.findByEmailAndPassword(user.getEmail(), user.getPassword());

		if (user2 != null) {
			response.setDetails(user2);
			response.setMsg("USER FOUND");
			response.setStatus(true);
			return response;
		} else {
			response.setDetails(null);
			response.setMsg("USER NOT FOUND");
			response.setStatus(false);
			return response;
		}

	}

}
