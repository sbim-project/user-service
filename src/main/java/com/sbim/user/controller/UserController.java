package com.sbim.user.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.print.attribute.HashAttributeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sbim.user.model.User;
import com.sbim.user.service.UserService;
import com.sbim.user.util.UserResponse;

@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	private UserService userService;

	@GetMapping("/")
	public String hi() {
		return "Hiii";
	}

	@GetMapping("/list")
	public ResponseEntity<List<User>> getAllUsers() {

		List<User> users = userService.getAllUsers();

		if (users != null) {
			return new ResponseEntity<>(users, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@GetMapping("/{id}")
	public ResponseEntity<User> getUserById(@PathVariable("id") String userid) {

		User user = userService.getUserById(userid);

		if (user != null) {
			return new ResponseEntity<User>(user, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@PostMapping("/save")
	public ResponseEntity<User> saveUser(@RequestBody User user) {

		User newuser = userService.saveUser(user);

		if (newuser != null) {
			return new ResponseEntity<>(newuser, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@PutMapping("/update")
	public ResponseEntity<User> updateUser(@RequestBody User user) {

		User updateduser = userService.updateUser(user);

		if (updateduser != null) {
			return new ResponseEntity<>(updateduser, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Map<String, String>> deleteUserById(@PathVariable("id") String userid) {

		Map<String, String> delete = new HashMap<>();

		String deletedid = userService.deleteUserById(userid);
		delete.put("deletedid", deletedid);

		if (deletedid != null) {
			return new ResponseEntity<>(delete, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@PostMapping("/verify")
	public ResponseEntity<UserResponse> verifyUser(@RequestBody User user) {

		UserResponse response = userService.verifyUser(user);

		return new ResponseEntity<UserResponse>(response, HttpStatus.OK);

	}

}
