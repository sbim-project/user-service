package com.sbim.user.util;

import com.sbim.user.model.User;

import lombok.Data;

@Data
public class UserResponse {
	
	String msg;
	
	Boolean status;
	
	User details;

}
