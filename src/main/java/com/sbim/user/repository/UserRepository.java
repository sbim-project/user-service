package com.sbim.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sbim.user.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String>{
	
	public User findByUserId(String userid);
	
	public User findByEmailAndPassword(String email, String password);

}
